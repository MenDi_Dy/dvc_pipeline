# Нужно реализовать насчет признак в рамках вашей гипотезы №1
def feature_1():
    pd_1 = send_sql_query("""
            WITH
            table_1 as (
                SELECT
                  building_id,
                  meter,
                  meter_reading
                FROM measurement_results
                        ),
            count_table as (
                SELECT
                  meter,
                COUNT(meter) AS count_met
                FROM measurement_results
                GROUP BY meter
                            )
            SELECT * 
            FROM table_1
            LEFT JOIN count_table ON table_1.meter = count_table.meter
                        """)
    
    return pd_1['count_met']
	
df1 = pd.DataFrame({'count_met':feature_1()})
df1.to_feather('../data/feature_1.fthr')