# Здесь нужно:
#1 объединить все насчитанные признаки с основной частью;
#2 Обучить любую модель, на любом кол-ве данных - качество не играет никакой роли 
# (пример можно поискать тут:https://www.kaggle.com/c/ashrae-energy-prediction/notebooks);
#3 Сохранить модель в pickle.
import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score, KFold
from sklearn.metrics import mean_squared_error


df_mod = pd.read_feather('base_part.fthr')
df1_mod = pd.read_feather('feature_1.fthr')
## df1_mod = pd.read_feather('feature_2.fthr') ## не прогружался на компьютере, компьютер зависал
total_dat = pd.concat([df_mod, df1_mod], axis=1)
total_dat.drop(['timestamp_measurement'], axis='columns', inplace=True)

xtrain, xvalid, ytrain, yvalid=train_test_split(total_dat.iloc[:,1:], total_dat['meter_reading'],
                                                test_size=0.15, random_state=241)
												
X = xtrain
y = ytrain
xgbr = xgb.XGBRegressor(max_depth = 5, n_estimators = 10)
kfold = KFold(n_splits=5, shuffle=True, random_state=241)
for train_index, test_index in kfold.split(X):
    X_train, X_test = X.iloc[train_index], X.iloc[test_index]
    y_train, y_test = y.iloc[train_index], y.iloc[test_index]
    xgbr.fit(X_train, y_train)
    scores = cross_val_score(xgbr, X_train, y_train, cv=kfold)
    print("Mean cross-validation score: %.2f" % scores.mean())
    ypred = xgbr.predict(X_test)
    mse = mean_squared_error(y_test, ypred)
    print("MSE: %.2f" % mse)