import pandas as pd
from helpers import send_sql_query
import sys

    def get_base_part():
    pd_1 = send_sql_query(""" SELECT meter_reading FROM measurement_results """)
    pd_2 = send_sql_query(""" SELECT building_id, meter FROM measurement_results """)
    pd_3 = send_sql_query(""" SELECT timestamp_measurement FROM measurement_results """)
    return pd.concat([pd_1, pd_2, pd_3], axis=1)
    
df = get_base_part()
df.to_feather('../data/base_part.fthr')
