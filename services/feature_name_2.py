# Нужно реализовать насчет признак в рамках вашей гипотезы №2
def feature_2():
    pd_1 = send_sql_query("""
            WITH
            table_1 as (
                SELECT
                  building_id,
                  meter,
                  timestamp_measurement,
                  meter_reading
                FROM measurement_results
                        ),
            count_table as (
                SELECT
                timestamp_measurement,
                dew_temperature,
                ABS(dew_temperature) AS abs_met
                FROM weather_train
                            )
            SELECT * 
            FROM table_1
            LEFT JOIN count_table ON table_1.timestamp_measurement = count_table.timestamp_measurement
                        """)
    
    return pd_1['abs_met']
	
df = pd.DataFrame({'abs_met':feature_2()})
df2.to_feather('../data/feature_2.fthr')